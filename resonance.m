%MATLAB R2020b
%name:resonance
%author:maciej_pekala
%date: 14.12.2020
%version: v1.0
clear; clc;

%wypisanie znanych wartości
r = 2.54/12/100; %promien drutu [cal na metr]
n = 40; %liczba zwoi
m = 0.1; %masa sprezyny [kg]
t_stala = 10; %stała czasowa [s]
g = 9.80665; %przyspieszenie ziemskie[m/s^2]
G = 8e10; %modul scinania [Pa]

R = [0.005:0.002:0.050]; %promien sprezyny [m]
f = [0:0.005:150]; %czestotliwosc [Hz]

%wpisanie do pliku
fprintf(wyniki_rezonans, "r = 2.54/12/100; %promien drutu [cal na metr]\n");
fprintf(wyniki_rezonans, "n = 40; %liczba zwoi\n");
fprintf(wyniki_rezonans, "m = 0.1; %masa sprezyny [kg]\n");
fprintf(wyniki_rezonans, "t_stala = 10; %stała czasowa [s]\n");
fprintf(wyniki_rezonans, "g = 9.80665; %przyspieszenie ziemskie[m/s^2]\n");
fprintf(wyniki_rezonans, "G = 8e10; %modul scinania [Pa]\n");
fprintf(wyniki_rezonans, "R = [0.005:0.002:0.050]; %promien sprezyny [m]\n");
fprintf(wyniki_rezonans, "f = [0:0.005:150]; %czestotliwosc [Hz]\n");

Fg = m*g;

%plik tekstowy do zapisu wyników
wyniki_rezonans = fopen('wyniki_rezonans.txt', 'w');

%wyliczenie wspolczynnika sprezystosci
k = (G*r^4)./(4*n*R.^3);

%wyliczenie czestosci wlasnej
w_0=sqrt(k/m);

% wyliczanie alfa 0
a_0= Fg/m;

%wyliczanie bety
B=1/(2*t_stala);

%wyliczanie omegi
w=2*pi*f;

%tablica do nadpisywania
tablica= ones(length(w),23);

%symulacja zmany amplitudy, symulacja dla roznych średnic
for i = 1:23
y= a_0./(sqrt((((w_0(i)^2)-w.^2).^2)+4*(B^2)*w.^2)); 
tablica(:, i)=y;
end

%stworzenie tablicy do nadpisywania
M_a= ones(23,1);

%zbieranie danych o maksymalnych amplitudach
for i = 1:23
M_a(i) = max(tablica(:,i));
    
%czectotliwosc maksymalnej amplitudy
c = find(tablica(:,i)==max(tablica(:,i)));

fprintf(wyniki_rezonans, "%.3f; %.2f; %.3f\n", R(i), M_a(i), f(c));
end


%tworzenie wykresow
Fig = figure;
subplot(2,1,1);
wykres_1 = plot(f,tablica);
xlabel('czestotliwosc wymuszajaca [Hz]');
ylabel('amplituda drgan sprezyny [m]');

subplot(2,1,2);
wykres_2 = plot(R,M_a);
xlabel('srednica sprezyny [m]');
ylabel('amplituda drgan [m]');



saveas(Fig, "wykresy.pdf")



